# Guestbook server #

NodeJS, Express, MongoDB based server for my wedding guestbook system. Receives greetings from a guest client and feeds (in realtime) a big screen client info about existing greetings.

Client code: https://bitbucket.org/moziz/vieraskirja-guest
