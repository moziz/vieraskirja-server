'use strict';
require('rootpath')();

const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const lessMiddleware = require('less-middleware');
const mongoose = require('mongoose');

const Config = require('config');

// Initialize MongoDB connection
mongoose.Promise = global.Promise;
const mongooseConnectPromise = mongoose.connect(Config.mongoConnectionURL, { useMongoClient: true });

mongooseConnectPromise.catch(err => {
  console.error("Mongo connection failed:", err);
  process.exit(1);

}).then(db => {
  db.on(
    'error',
    console.error.bind(console, "connection error:")
  );

  db.once(
    'open',
    () => {
      console.log("MongoDB connection successful");
    }
  );
});

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', express.static(Config.clientPath));
app.use('/data', express.static('data'));
app.use('/api/display', require('routes/display'));
app.use('/api/submit', require('routes/submit'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
