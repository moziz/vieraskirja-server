/**
 * Created by janne on 10.10.2016.
 */
'use strict';

//const lodash = require('lodash');

class WSMessage {
  /**
   *
   * @param {?Error} error - Set to null if all is fine otherwise put the error object here
   * @param {String} topic - Identifying piece of information shared between the client and the server. Used to link
   *  requests and replies together.
   * @param {?Object} data - Payload data object.
   */
  constructor(
    error,
    topic,
    data
  ) {
    this.error = error;
    this.topic = topic;
    this.data = data;
  }

  toString() {
    return JSON.stringify(
      this,
      (key, value) => {
        if (value instanceof Error) {
          let error = {};
          error.message = value.message;
          return error;
        }

        return value;
      }
    );
  }

  get json() {
    return this.toString();
  }

  /**
   * Attempt to parse the input string into a WSMessage object
   * @param {String} source - JSON string
   * @returns {?WSMessage} - Upon success, a WSMessage is returned. Null if parsing failed.
   */
  static parse(source) {
    let json = null;

    try {
      json = JSON.parse(source);
    } catch (e) {
      console.log("Parsing client message failed:", e);
      return null;
    }

    /*
    // TODO Replace this nonsense with proper validation
    let error = json['error'];
    let topic = json['topic'];
    let data = json['data'];

    if (!lodash.isObject(error) ||
      !lodash.isString(topic) ||
      !lodash.isObject(data)
    ) {
      return null;
    }
    */

    return new WSMessage(json.error, json.topic, json.data);
  }
}

module.exports = WSMessage;
