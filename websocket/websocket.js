"use strict";

const WS = require('ws');

const WSMessage = require('websocket/wsmessage');

let wss = null;
let openConnections = [];

module.exports = {
  init: (httpServer) => {
    wss = new WS.Server({ server: httpServer });

    wss.on('connection', (ws, req) => {
      console.log("WebSocket connection request received.");

      openConnections.push(ws);

      ws.onopen = ()=> {
        console.log("WebSocket connection opened.");
      };

      ws.onclose = (code, reason) => {
        console.log("WebSocket connection closed.");
      };

      ws.onmessage = (message) => {
        console.log("Got WebSocket message:", message);
      };
    });
  },

  // message: WSMessage
  broadcast: (message) => {
    for (let i = openConnections.length; i-- > 0;) {
      const ws = openConnections[i];
      if (ws.readyState !== 1) {
        // Not open
        continue;
      }

      try {
        ws.send(message.json);
      } catch (e) {
        console.error("WebSocket broadcast exception:", e);
      }
    }
  }
};
