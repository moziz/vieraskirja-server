'use strict';

const mongoose = require('mongoose');


const schema = mongoose.Schema({
  date: { type: Date, required: true, index: true },
  fileIdentifier: { type: String, required: true, index: true, unique: true },
  userName: { type: String, required: true },
  drawingName: { type: String, required: true },
  bodyText: { type: String, required: true }
});

const model = mongoose.model('Card', schema);

module.exports = model;
