'use strict';

const mongoose = require('mongoose');


const schema = mongoose.Schema({
  text: { type: String, required: true, trim: true }
});

const model = mongoose.model('Prompt', schema);

module.exports = model;
