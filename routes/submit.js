"use strict";

const express = require('express');
const router = express.Router();
const multer = require('multer');

const Config = require('config');
const WebSocket = require('websocket/websocket');
const WSMessage = require('websocket/wsmessage');
const Card = require('model/card');

// Setup multer
const multerStorage = multer.diskStorage({
  destination: 'data/drawings',
  filename: (req, file, cb) => {
    if (!req.fileIdentifier) {
      req.fileIdentifier = Date.now();
    }

    cb(null, req.fileIdentifier + '.' + file.fieldname + '.png');
  }
});
const multerUpload = multer({
  storage: multerStorage
});
const multerUploadCard = multerUpload.fields([
  { name: 'avatar', maxCount: 1 },
  { name: 'drawing', maxCount: 1 }
]);


router.post(
  '/card',
  multerUploadCard,
  (err, req, res, next) => {
    if (err) {
      console.error("Error while handling a card upload:", err);
    }

    next(err);
  },
  (req, res) => {
    if (!req.files) {
      res.status(500).send({
        error: true,
        msg: "File creation failed"
      });

      return;
    }

    let userName = req.body['userName'];
    if (!userName) userName = 'Anonyymi';

    let drawingName = req.body['drawingName'];
    if (!drawingName) drawingName = '???';

    let bodyText = req.body['bodyText'];
    if (!bodyText) bodyText = '...';

    const card = new Card({
      date: new Date(),
      fileIdentifier: req.fileIdentifier,
      userName: userName,
      drawingName: drawingName,
      bodyText: bodyText
    });

    card.save(err => {
      if (err) {
        let msg = "Saving card to the database failed:";
        console.error(msg, err);
        res.status(500).send({
          error: err,
          msg: msg
        });

        return;
      }

      res.status(200).send({
        error: null,
        msg: "Card accepted"
      });

      WebSocket.broadcast(new WSMessage(null, 'new-card', { id: card._id }));
    });
  }
);

module.exports = router;
