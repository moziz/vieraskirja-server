"use strict";

const express = require('express');
const router = express.Router();

const Config = require('config');
const Card = require('model/card');
const Prompt = require('model/prompt');

router.get('/card/all', (req, res) => {
  Card.find({}, {}, {}, (err, docs) => {
    if (err)
    {
      res.status(500).send(err);
    }

    res.status(200).send(docs);
  });
});

router.get('/card/:id', (req, res) => {
  Card.findOne({ _id: req.params.id }, {}, {}, (err, doc) => {
    if (err)
    {
      res.status(500).send(err);
    }

    res.status(200).send(doc);
  });
});

router.get('/prompt/all', (req, res) => {
  Prompt.find({}, {}, {}, (err, docs) => {
    if (err)
    {
      res.status(500).send(err);
    }

    res.status(200).send(docs);
  });
});

module.exports = router;
